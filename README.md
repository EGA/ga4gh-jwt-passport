# GA4GH JWT Passport Library
Core library to generate GA4GH JWT Passports.

### E.g.
```
   HEADER:
   {
	"jku": "https://ega.ebi.ac.uk:8053/ega-openid-connect-server/jwk",
	"kid": "rsa1",
	"typ": "JWT",
	"alg": "RS256"
   }
    
   PAYLOAD: 
   {
	"sub": "EGAW00000012231",
	"ga4gh_visa_v1": {
		"type": "ControlledAccessGrants",
		"asserted": 1618309037,
		"value": "https://ega-archive.org/datasets/EGAD00001003338",
		"source": "https://ega-archive.org/dacs/EGAC00001000620",
		"by": "dac"
	},
	"iss": "https://ega.ebi.ac.uk:8053/ega-openid-connect-server/",
	"exp": 1620394509,
	"iat": 1620390909,
	"jti": "7a5154c6-4610-46ad-91e6-60014f0222b3"
   }
```
