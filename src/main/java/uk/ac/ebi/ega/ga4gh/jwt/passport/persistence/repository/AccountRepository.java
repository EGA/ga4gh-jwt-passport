package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.Account;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, String> {
    Optional<Account> findByEmail(String email);
}
