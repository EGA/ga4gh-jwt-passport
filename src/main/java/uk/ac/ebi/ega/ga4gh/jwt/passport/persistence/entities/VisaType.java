package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities;

public enum VisaType {
    AffiliationAndRole, ControlledAccessGrants, AcceptedTermsAndPolicies, ResearcherStatus, LinkedIdentities
}
