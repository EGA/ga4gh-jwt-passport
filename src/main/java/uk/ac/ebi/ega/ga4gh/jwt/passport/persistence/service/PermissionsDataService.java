package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.service;

import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.Account;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.AccountElixirId;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.PassportClaim;

import java.util.List;
import java.util.Optional;

public interface PermissionsDataService {

    List<PassportClaim> getPassportClaimsForAccount(String userAccountId);

    List<PassportClaim> getPassportClaimsForAccountAndController(String userAccountId, String controllerAccountId);

    Optional<Account> getAccountByEmail(String email);

    Optional<AccountElixirId> getAccountIdForElixirId(String elixirId);

    PassportClaim savePassportClaim(PassportClaim claim);

    Optional<PassportClaim> deletePassportClaim(String accountId, String value);

    boolean accountExists(String accountId);

    List<PassportClaim> getPassportClaimsForDataset(String datasetId);

    List<PassportClaim> getPassportClaimsByUserAndController(String accountId, String egaAccountStableId);

    boolean userCanControlDataset(String controllerAccountId, String value);
}
