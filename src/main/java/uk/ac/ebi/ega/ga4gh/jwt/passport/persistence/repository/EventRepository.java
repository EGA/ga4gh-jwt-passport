package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.Event;

public interface EventRepository extends CrudRepository<Event, Long> {

}
