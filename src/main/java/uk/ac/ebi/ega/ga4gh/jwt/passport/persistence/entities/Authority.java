package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities;

public enum Authority {
    dac, system
}