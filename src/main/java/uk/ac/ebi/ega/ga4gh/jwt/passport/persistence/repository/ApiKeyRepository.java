/*
 * Copyright 2021 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.ApiKey;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.ApiKeyId;

import java.util.List;
import java.util.Optional;

public interface ApiKeyRepository extends CrudRepository<ApiKey, ApiKeyId> {

	List<ApiKey> findAllByApiKeyIdUsername(String username);

	void removeAllByApiKeyIdUsernameAndApiKeyIdKeyName(String username, String keyName);

	Optional<ApiKey> findApiKeyByApiKeyIdUsernameAndApiKeyIdKeyName(String username, String keyName);
}
