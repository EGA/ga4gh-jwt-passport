/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.service;

import com.nimbusds.jwt.SignedJWT;
import uk.ac.ebi.ega.ga4gh.jwt.passport.model.IJWTData;

public interface IJWTService {
    /**
     * Creates SignedJWT object from IJWTData object
     *
     * @param jwtData IJWTData object
     *
     * @return SignedJWT object
     */
    SignedJWT createJWT(IJWTData jwtData);

    /**
     * Signs SignedJWT object
     *
     * @param signedJWT SignedJWT object to be signed
     */
    void signJWT(SignedJWT signedJWT);

    /**
     * Validates signature
     *
     * @param signedJWT SignedJWT object to be validated
     *
     * @return true if valid signature else false
     */
    boolean isValidSignature(SignedJWT signedJWT);
}
