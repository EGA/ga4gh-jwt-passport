package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@TypeDef(name = "visa_type", typeClass = PostgreSQLEnumType.class)
@TypeDef(name = "visa_authority", typeClass = PostgreSQLEnumType.class)
public class PassportClaim {

	@EmbeddedId
	private PassportClaimId passportClaimId;

	@Enumerated(EnumType.STRING)
	@Type(type = "visa_type")
	private VisaType type;

	private Long asserted;

	private String source;

	@Enumerated(EnumType.STRING)
	@Type(type = "visa_authority")
	private Authority by;

	private String status = "approved";

	public PassportClaim() {

	}

	public PassportClaim(PassportClaimId passportClaimId, VisaType type, Long asserted, String source, Authority by) {
		this.passportClaimId = passportClaimId;
		this.type = type;
		this.asserted = asserted;
		this.source = source;
		this.by = by;
	}

	public PassportClaimId getPassportClaimId() {
		return passportClaimId;
	}

	public void setPassportClaimId(PassportClaimId passportClaimId) {
		this.passportClaimId = passportClaimId;
	}

	public VisaType getType() {
		return type;
	}

	public void setType(VisaType type) {
		this.type = type;
	}

	public Long getAsserted() {
		return asserted;
	}

	public void setAsserted(Long asserted) {
		this.asserted = asserted;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Authority getBy() {
		return by;
	}

	public void setBy(Authority by) {
		this.by = by;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
