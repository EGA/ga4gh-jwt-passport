package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "USER_GROUP")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class AccessGroup {
	@EmbeddedId
	private AccessGroupId accessGroupId;

	@Enumerated(EnumType.STRING)
	@Type(type = "pgsql_enum")
	@Column(name = "group_type")
	private GroupType groupType;

	@Enumerated(EnumType.STRING)
	@Type(type = "pgsql_enum")
	@Column(name = "permission")
	private Permission permission;

	private String status;

	private int peaRecord;

	public AccessGroup() {
	}

	public AccessGroup(AccessGroupId accessGroupId, GroupType groupType, Permission permission) {
		this.accessGroupId = accessGroupId;
		this.groupType = groupType;
		this.permission = permission;
		this.status = "approved";
		this.peaRecord = 0;
	}

	public AccessGroupId getAccessGroupId() {
		return accessGroupId;
	}

	public void setAccessGroupId(AccessGroupId accessGroupId) {
		this.accessGroupId = accessGroupId;
	}

	public GroupType getGroupType() {
		return groupType;
	}

	public void setGroupType(GroupType groupType) {
		this.groupType = groupType;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPeaRecord() {
		return peaRecord;
	}

	public void setPeaRecord(int peaRecord) {
		this.peaRecord = peaRecord;
	}
}
