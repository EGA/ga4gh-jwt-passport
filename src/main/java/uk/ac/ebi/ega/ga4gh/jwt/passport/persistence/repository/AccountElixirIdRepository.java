package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.AccountElixirId;

import java.util.Optional;

public interface AccountElixirIdRepository extends CrudRepository<AccountElixirId, String> {

    Optional<AccountElixirId> findByElixirId(String elixirId);

}
