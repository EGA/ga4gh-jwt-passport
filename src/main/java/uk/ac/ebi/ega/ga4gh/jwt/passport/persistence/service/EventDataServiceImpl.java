package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.service;

import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.Event;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.repository.EventRepository;

public class EventDataServiceImpl implements EventDataService {
    private EventRepository eventRepository;

    public EventDataServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public Event saveEvent(Event event) {
        return eventRepository.save(event);
    }
}
