/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.model;

import uk.ac.ebi.ega.ga4gh.jwt.passport.constants.GA4GHVisaType;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class GA4GHVisaClaim {

    private static final String GRANT_TYPE = "type";
    private static final String ASSERTION_TIME = "asserted";
    private static final String VALUE = "value";
    private static final String SOURCE = "source";
    private static final String APPROVED_BY = "by";

    private final Map<String, Object> ga4ghClaims = new LinkedHashMap<>();

    private GA4GHVisaClaim(final Map<String, Object> ga4ghClaims) {
        this.ga4ghClaims.putAll(ga4ghClaims);
    }

    public static class Builder {

        private final Map<String, Object> ga4ghClaims = new LinkedHashMap<>();

        public Builder() {
        }

        public Builder grantType(final GA4GHVisaType grantType) {
            this.ga4ghClaims.put(GRANT_TYPE, grantType.getName());
            return this;
        }

        public Builder assertionTime(final long assertionTime) {
            this.ga4ghClaims.put(ASSERTION_TIME, assertionTime);
            return this;
        }

        public Builder value(final String value) {
            this.ga4ghClaims.put(VALUE, value);
            return this;
        }

        public Builder source(final String source) {
            this.ga4ghClaims.put(SOURCE, source);
            return this;
        }

        public Builder approvedBy(final String approvedBy) {
            this.ga4ghClaims.put(APPROVED_BY, approvedBy);
            return this;
        }

        public GA4GHVisaClaim build() {
            return new GA4GHVisaClaim(ga4ghClaims);
        }
    }

    public Map<String, Object> toGa4ghClaimMap() {
        return Collections.unmodifiableMap(ga4ghClaims);
    }
}
