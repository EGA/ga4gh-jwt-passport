package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.PassportClaim;
import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.PassportClaimId;

import java.util.List;
import java.util.Optional;

public interface PassportClaimRepository extends CrudRepository<PassportClaim, PassportClaimId> {

    @Query("select pc from PassportClaim pc where pc.passportClaimId.accountId=:accountId and pc.status='approved'")
    List<PassportClaim> findAllByAccountId(@Param("accountId") String accountId);

    @Query("select pc from PassportClaim pc" +
            " inner join Dataset ds on pc.passportClaimId.value = ds.datasetId" +
            " inner join AccessGroup ug on ds.dacStableId=ug.accessGroupId.groupStableId" +
            " where ug.accessGroupId.egaAccountStableId=:controllerAccountId and pc.passportClaimId.accountId=:userAccountId and pc.status='approved'")
    List<PassportClaim> findAllByAccountIdAndControllerId(@Param("userAccountId") String userAccountId,
                                                          @Param("controllerAccountId") String controllerAccountId);

    @Query("select case when count(pc)> 0 then true else false end from PassportClaim pc " +
            "where pc.passportClaimId.accountId=:accountId and pc.status='approved'")
    boolean existsPassportClaimByAccountId(@Param("accountId") String accountId);

    @Query("select pc from PassportClaim pc " +
            "where pc.passportClaimId.accountId=:accountId and pc.passportClaimId.value=:value and pc.status='approved'")
    Optional<PassportClaim> findByAccountIdAndValue(@Param("accountId") String accountId, @Param("value") String value);

    @Query("select pc from PassportClaim pc where pc.passportClaimId.value=:value and pc.status='approved'")
    List<PassportClaim> findAllByValue(@Param("value") String value);

    @Query("select pc from PassportClaim pc" +
            " inner join Dataset ds on ds.datasetId=pc.passportClaimId.value" +
            " inner join AccessGroup ug on ug.accessGroupId.groupStableId=ds.dacStableId" +
            " where pc.passportClaimId.accountId=:accountId and ug.accessGroupId.egaAccountStableId=:egaAccountStableId")
    List<PassportClaim> findAllByUserAndController(@Param("accountId") String accountId, @Param("egaAccountStableId") String egaAccountStableId);
}
