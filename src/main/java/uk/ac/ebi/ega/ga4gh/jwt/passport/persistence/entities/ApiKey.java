/*
 * Copyright 2021 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.Date;

@Entity
public class ApiKey {

	@EmbeddedId
	private ApiKeyId apiKeyId;

	private Date expiration;
	private String reason;
	private String salt;
	private String privateKey;

	public ApiKey() {
	}

	public ApiKey(ApiKeyId apiKeyId, Date expiration, String reason, String salt, String privateKey) {
		this.apiKeyId = apiKeyId;
		this.expiration = expiration;
		this.reason = reason;
		this.salt = salt;
		this.privateKey = privateKey;
	}

	public ApiKeyId getApiKeyId() {
		return apiKeyId;
	}

	public void setApiKeyId(ApiKeyId apiKeyId) {
		this.apiKeyId = apiKeyId;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
}
