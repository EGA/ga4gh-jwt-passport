package uk.ac.ebi.ega.ga4gh.jwt.passport.exception;

public class ServiceException extends RuntimeException {

    public ServiceException(String message){
        super(message);
    }
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
