package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.service;

import uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities.Event;

public interface EventDataService {
    Event saveEvent(Event events);
}
