package uk.ac.ebi.ega.ga4gh.jwt.passport.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Dataset {
	@Id
	private String datasetId;
	private String description;
	private String dacStableId;
	private String doubleSignature;

	public Dataset() {
	}

	public Dataset(String datasetId, String description, String dacStableId, String doubleSignature) {
		this.datasetId = datasetId;
		this.description = description;
		this.dacStableId = dacStableId;
		this.doubleSignature = doubleSignature;
	}

	public String getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDacStableId() {
		return dacStableId;
	}

	public void setDacStableId(String dacStableId) {
		this.dacStableId = dacStableId;
	}

	public String getDoubleSignature() {
		return doubleSignature;
	}

	public void setDoubleSignature(String doubleSignature) {
		this.doubleSignature = doubleSignature;
	}
}
