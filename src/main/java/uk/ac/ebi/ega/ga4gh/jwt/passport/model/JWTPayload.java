/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.model;

import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;

import java.text.ParseException;
import java.util.Map;

public class JWTPayload {

    private static final String ISSUER = "iss";
    private static final String SUBJECT = "sub";
    private static final String EXPIRATION_TIME = "exp";
    private static final String ISSUED_AT = "iat";
    private static final String JWT_ID = "jti";
    private static final String GA4GH_CLAIMS = "ga4gh_visa_v1";

    private final JSONObject payload = new JSONObject();

    private JWTPayload(final Map<String, Object> payload) {
        this.payload.putAll(payload);
    }

    public static class Builder {

        private final JSONObject payload = new JSONObject();

        public Builder() {
        }

        public Builder issuer(final String issuer) {
            payload.put(ISSUER, issuer);
            return this;
        }

        public Builder subject(final String subject) {
            payload.put(SUBJECT, subject);
            return this;
        }

        public Builder expirationTime(final long expirationTime) {
            payload.put(EXPIRATION_TIME, expirationTime);
            return this;
        }

        public Builder issueTime(final long issueTime) {
            payload.put(ISSUED_AT, issueTime);
            return this;
        }

        public Builder jwtID(final String jwtId) {
            payload.put(JWT_ID, jwtId);
            return this;
        }

        public Builder ga4ghClaims(final Object ga4ghClaims) {
            payload.put(GA4GH_CLAIMS, ga4ghClaims);
            return this;
        }

        public JWTPayload build() {
            return new JWTPayload(payload);
        }
    }

    public JWTClaimsSet toJWTClaimsSet() throws ParseException {
        return JWTClaimsSet.parse(payload);
    }
}
