/*
 *
 * Copyright 2020 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ResourceUtils;
import uk.ac.ebi.ega.ga4gh.jwt.passport.constants.JWTAlgorithm;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.stream.Collectors;

@TestConfiguration
public class JWTConfigTest {
    @Bean
    public IJWTService initJWTService(@Value("${jwks.keystore.path}") String jwksKeystorePath,
                                      @Value("${jwks.signer.default-key.id}") String defaultSignerKeyId,
                                      @Value("${jwks.url}") String jwksURL,
                                      @Value("${token.expire.time}") long tokenExpireTime,
                                      @Value("${issuer.url}") String issuerURL) throws IOException, ParseException, URISyntaxException {
        final File keystoreFile = ResourceUtils.getFile(jwksKeystorePath);
        assertFileExistsAndReadable(keystoreFile, "Keystore file should exists & must be readable");

        final String jwks = Files.lines(keystoreFile.toPath()).collect(Collectors.joining());
        return new JWTService(
                jwks,
                defaultSignerKeyId,
                JWTAlgorithm.RS256,
                tokenExpireTime,
                new URL(jwksURL),
                new URL(issuerURL));
    }

    private void assertFileExistsAndReadable(final File file, final String message) throws FileSystemException {
        if (!file.canRead()) {
            throw new FileSystemException(message);
        }
    }
}
