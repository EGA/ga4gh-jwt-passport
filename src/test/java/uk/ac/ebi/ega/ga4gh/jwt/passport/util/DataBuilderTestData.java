/*
 *
 * Copyright 2020 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.util;


import uk.ac.ebi.ega.ga4gh.jwt.passport.model.IJWTData;
import uk.ac.ebi.ega.ga4gh.jwt.passport.model.JWTData;

import static java.lang.String.format;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.constants.GA4GHVisaType.CONTROLLED_ACCESS_GRANTS;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.constants.GA4GHVisaType.LINKED_IDENTITIES;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.constants.VisaApprovedAuthority.DAC;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.constants.VisaApprovedAuthority.SYSTEM;

public interface DataBuilderTestData {
    String EGA_ACCOUNT_ID = "EGAW00000000001";
    String DATASET_ID = "EGAD00000000001";
    String DAC_ID = "EGAC00000000001";
    String ELIXIR_ID = "567e0e9ret779dca86b4132485bcfe4waca32db6@elixir-europe.org";

    static IJWTData buildJWTDataDatasetPermission() {
        return new JWTData(
                format("https://ega-archive.org/datasets/%s", DATASET_ID),
                format("https://ega-archive.org/dacs/%s", DAC_ID),
                1568814383,
                DAC.getName(),
                EGA_ACCOUNT_ID,
                CONTROLLED_ACCESS_GRANTS
        );
    }

    static IJWTData buildJWTDataLinkedIdentity() {
        return new JWTData(
                ELIXIR_ID + ",https%3A%2F%2Flogin.elixir-czech.org%2Foidc%2F",
                "https://ega-archive.org/",
                1568814383,
                SYSTEM.getName(),
                EGA_ACCOUNT_ID,
                LINKED_IDENTITIES
        );
    }
}
