/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.ga4gh.jwt.passport.service;

import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.SignedJWT;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import uk.ac.ebi.ega.ga4gh.jwt.passport.constants.GA4GHVisaType;
import uk.ac.ebi.ega.ga4gh.jwt.passport.model.IJWTData;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.constants.VisaApprovedAuthority.DAC;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.constants.VisaApprovedAuthority.SYSTEM;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.util.DataBuilderTestData.DAC_ID;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.util.DataBuilderTestData.DATASET_ID;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.util.DataBuilderTestData.EGA_ACCOUNT_ID;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.util.DataBuilderTestData.ELIXIR_ID;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.util.DataBuilderTestData.buildJWTDataDatasetPermission;
import static uk.ac.ebi.ega.ga4gh.jwt.passport.util.DataBuilderTestData.buildJWTDataLinkedIdentity;


@TestPropertySource("classpath:application-test.properties")
@Import(JWTConfigTest.class)
@ContextConfiguration
@RunWith(SpringRunner.class)
public class JWTServiceTest {

    //JWT properties
    private static final String ISSUER = "iss";
    private static final String SUBJECT = "sub";
    private static final String EXPIRATION_TIME = "exp";
    private static final String ISSUED_AT = "iat";
    private static final String JWT_ID = "jti";
    private static final String GA4GH_CLAIMS = "ga4gh_visa_v1";

    //GA4GH Visa/Claims properties
    private static final String GRANT_TYPE = "type";
    private static final String ASSERTION_TIME = "asserted";
    private static final String VALUE = "value";
    private static final String SOURCE = "source";
    private static final String APPROVED_BY = "by";

    @Value("${jwks.url}")
    private String jwksURL;

    @Value("${dataset.url}")
    private String datasetURL;

    @Value("${dac.url}")
    private String dacURL;

    @Value("${issuer.url}")
    private String issuerURL;

    @Value("${elixir.aai.url}")
    private String elixirAAIURL;

    @Value("${ega.url}")
    private String egaURL;

    @Value("${jwks.signer.default-key.id}")
    private String defaultSignerKeyId;

    @Autowired
    private IJWTService jwtService;

    @Test
    public void createJWT_ReturnsDatasetSignedJWTObject() throws URISyntaxException {
        //Given: build jwt object
        final IJWTData jwtData = buildJWTDataDatasetPermission();

        //Test: following method to be tested
        final SignedJWT jwt = jwtService.createJWT(jwtData);

        //Assertions: assert SignedJWT object's data
        assertThat(jwt).isNotNull();
        assertThat(jwt.getPayload()).isNotNull();

        final JSONObject jsonObject = jwt.getPayload().toJSONObject();
        assertThat(jsonObject).isNotNull();

        assertThat(new URI(issuerURL).normalize().toString()).isEqualTo(jsonObject.get(ISSUER));

        assertThat(jsonObject.get(SUBJECT)).isEqualTo(EGA_ACCOUNT_ID);
        assertThat(jsonObject.get(EXPIRATION_TIME)).isNotNull();
        assertThat(jsonObject.get(ISSUED_AT)).isNotNull();
        assertThat(jsonObject.get(JWT_ID)).isNotNull();
        assertThat(jsonObject.get(GA4GH_CLAIMS)).isNotNull();


        final Map<String, Object> ga4GhVisaMap = Collections.unmodifiableMap((Map<String, Object>) jsonObject.get("ga4gh_visa_v1"));
        assertThat(ga4GhVisaMap.isEmpty()).isFalse();

        assertThat(ga4GhVisaMap.get(GRANT_TYPE)).isEqualTo(GA4GHVisaType.CONTROLLED_ACCESS_GRANTS.getName());
        assertThat(ga4GhVisaMap.get(ASSERTION_TIME)).isNotNull();

        assertThat(ga4GhVisaMap.get(VALUE).toString()).isEqualTo(new URI(datasetURL).normalize().resolve(DATASET_ID).toString());
        assertThat(ga4GhVisaMap.get(SOURCE).toString()).isEqualTo(new URI(dacURL).normalize().resolve(DAC_ID).toString());
        assertThat(ga4GhVisaMap.get(APPROVED_BY)).isEqualTo(DAC.getName());

        assertThat(jwt.getHeader()).isNotNull();
        final JWSHeader header = jwt.getHeader();

        assertThat(header.getJWKURL()).isEqualTo(new URI(jwksURL));
        assertThat(header.getKeyID()).isEqualTo(defaultSignerKeyId);
        assertThat(header.getAlgorithm().getName()).isEqualTo("RS256");

        assertThat(jwt.getSignature()).isNull();
    }

    @Test
    public void createJWT_ReturnsEgaElixirMappingSignedJWTObject() throws URISyntaxException, UnsupportedEncodingException {
        //Given: build jwt object
        final IJWTData jwtData = buildJWTDataLinkedIdentity();

        //Test: following method to be tested
        final SignedJWT jwt = jwtService.createJWT(jwtData);

        //Assertions: assert SignedJWT object's data
        assertThat(jwt).isNotNull();
        assertThat(jwt.getPayload()).isNotNull();

        final JSONObject jsonObject = jwt.getPayload().toJSONObject();
        assertThat(jsonObject).isNotNull();

        assertThat(jsonObject.get(ISSUER)).isEqualTo(new URI(issuerURL).normalize().toString());
        assertThat(jsonObject.get(SUBJECT)).isEqualTo(EGA_ACCOUNT_ID);
        assertThat(jsonObject.get(EXPIRATION_TIME)).isNotNull();
        assertThat(jsonObject.get(ISSUED_AT)).isNotNull();
        assertThat(jsonObject.get(JWT_ID)).isNotNull();
        assertThat(jsonObject.get(GA4GH_CLAIMS)).isNotNull();

        final Map<String, Object> ga4GhVisaMap = Collections.unmodifiableMap((Map<String, Object>) jsonObject.get("ga4gh_visa_v1"));
        assertThat(ga4GhVisaMap.isEmpty()).isFalse();

        assertThat(ga4GhVisaMap.get(GRANT_TYPE)).isEqualTo(GA4GHVisaType.LINKED_IDENTITIES.getName());
        assertThat(ga4GhVisaMap.get(ASSERTION_TIME)).isNotNull();

        final String encodedElixirAAIURL = String.join(",", ELIXIR_ID, URLEncoder.encode(elixirAAIURL, "UTF-8"));

        assertThat(ga4GhVisaMap.get(VALUE).toString()).isEqualTo(encodedElixirAAIURL);
        assertThat(ga4GhVisaMap.get(SOURCE).toString()).isEqualTo(egaURL);
        assertThat(ga4GhVisaMap.get(APPROVED_BY)).isEqualTo(SYSTEM.getName());

        assertThat(jwt.getHeader()).isNotNull();
        final JWSHeader header = jwt.getHeader();
        assertThat(header.getJWKURL()).isEqualTo(new URI(jwksURL));
        assertThat(header.getKeyID()).isEqualTo(defaultSignerKeyId);
        assertThat(header.getAlgorithm().getName()).isEqualTo("RS256");

        assertThat(jwt.getSignature()).isNull();
    }

    @Test
    public void signJWT_SignsJWTTokenUsingSigner() {
        //Given: build jwt object
        final IJWTData jwtData = buildJWTDataLinkedIdentity();

        //Test: following method to be tested
        final SignedJWT jwt = jwtService.createJWT(jwtData);
        //Assertions: assert SignedJWT signature
        assertThat(jwt.getSignature()).isNull();

        //sign jwt
        jwtService.signJWT(jwt);
        //Assertions: assert SignedJWT signature
        assertThat(jwt.getSignature()).isNotNull();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void verifySignature_ThrowsOperationNotSupported() {
        //Given: build jwt object
        final IJWTData jwtData = buildJWTDataLinkedIdentity();

        //Create JWT token
        final SignedJWT jwt = jwtService.createJWT(jwtData);

        //Test: following method to be tested
        jwtService.isValidSignature(jwt);
    }
}
